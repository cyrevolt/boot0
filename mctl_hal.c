
void set_ddr_voltage(unsigned val)
{
	return;
}

void handler_super_standby()
{
	return;
}

int get_pmu_exist()
{
	return -1;
}

char* memcpy_self(char* dst, char* src, int len)
{
	int i;
	for(i=0; i!=len; i++) {
		dst[i] = src[i];
	}
	return dst;
}

void dram_udelay(uint32_t d1)
{
	uint64_t d2 = d1;
	__usdelay(d2);
	return;
}

void dram_vol_set(__dram_para_t *para)
{
	int reg, vol = 0;
	
	switch( para->dram_type ) {
	case 2:  vol = 47; break;
	case 3:  vol = 25; break;
	default: vol = 0;
	}

	reg = readl(0x3000150);
	reg &= ~(0xff00);
	reg |= a4 << 8;
	reg &= ~(0x200000);
	writel(0x3000150, reg);
	
	__usdelay(1);
	fence_ir();	
}

void paraconfig(unsigned int *para, unsigned int mask, unsigned int value)
{
	*para &= ~(mask);
	*para |= value;
}


int dram_enable_all_master()
{
	writel(0x3102020, -1);
	writel(0x3102024, 0xff);
	writel(0x3102028, 0xffff);
	__usdelay(10);
}


int dram_disable_all_master()
{
	writel(0x3102020, 1);
	writel(0x3102024, 0);
	writel(0x3102028, 0);
	__usdelay(10);
}


int eye_delay_compensation(__dram_para_t *para)
{
}

// Not used ??
//
void bit_delay_compensation()
{
	const unsigned int data0[44] = {
		0, 1, 2, 3, 2, 3, 3, 3, 0, 0, 0,
		6, 6, 6, 5, 5, 5, 5, 5, 0, 0, 0,
		0, 2, 4, 2, 6, 5, 5, 5, 0, 0, 0,
		3, 3, 3, 2, 2, 1, 1, 1, 0, 0, 0,
	};
	const unsigned int data1[44] = {
		0, 1, 3, 3, 3, 3, 3, 4, 3, 3, 3,
		3, 3, 4, 4, 3, 3, 3, 3, 3, 3, 3,
		0, 3, 3, 1, 6, 6, 5, 6, 3, 3, 3,
		5, 5, 6, 6, 4, 5, 3, 3, 3, 3, 3,
	}
	
	rval = readl((void*)0x3102100) & 0x03ffffff;
	writel((void*)0x3102100), rval);

	unsigned int *start = (unsigned int *)0x3102310; // DATX0IOCR
	unsigned int *end   = (unsigned int *)0x3102510; // DATX0IOCR x + 4 * size
	unsigned int *datxiocr;
	unsigned int i, j, k, rval;
	
	// Fill DATX0IOCR - DATX3IOCR, 11 registers per block, blocks 0x20 words apart
	for(i = 0, datxiocr = start; datxiocr != end; i += 11; datxiocr += 0x20) {
		for(j = 0, k = i; j != 11; j++, k++) {
			rval  = readl(datxiocr[j]);
			rval += data1[k] << 8 + data0[k];
			writel(datxiocr[j], rval);
		}
	}
	
	rval = readl((void*)0x3102100) | 0x04000000;
	writel((void*)0x3102100), rval);
}


int set_master_priority_pad(uint32_t *p)
{
	uint32_t val;
	
	val = readl(0x310200c) & 0xfffff000;
	val |= (p[0] >> 1) - 1;
	writel(0x310200c, val);
	
	writel(0x3102200, 0x00001000);
	writel(0x3102210, 0x01000009);
	writel(0x3102214, 0x00500100);
	writel(0x3102230, 0x0200000d);
	writel(0x3102234, 0x00600100);
	writel(0x3102240, 0x01000009);
	writel(0x3102244, 0x00500100);
	writel(0x3102260, 0x00640209);
	writel(0x3102264, 0x00200040);
	writel(0x3102290, 0x01000009);
	writel(0x3102294, 0x00400080);
	writel(0x3102470, 0);
	writel(0x3102474, 0);
	
	writel(0x31031c0, 0x0f802f05);
	writel(0x31031c8, 0x0f0000ff);
	writel(0x31031d0, 0x3f00005f);		
}

int auto_cal_timing(uint32_t time, uint32_t freq)
{
	uint32_t t = time*freq;
	return t/1000 + ( ((t%1000) != 0) ? 1 : 0);
}


void auto_set_timing_para(__dram_para_t *para) // s5
{
	unsigned int   freq;	// s4
	unsigned int   type;	// s8
	unsigned int   tpr13;	// 80(sp)
	
	unsigned char  tccd;	// 88(sp)
	unsigned char  trrd;	// s7
	unsigned char  trcd;	// s3
	unsigned char  trc;	// s9
	unsigned char  tfaw;	// s10
	unsigned char  tras;	// s11
	unsigned char  trp;	// 0(sp)
	unsigned char  twtr;	// s1
	unsigned char  twr;	// s6
	unsigned char  trtp;	// 64(sp)
	unsigned char  txp;	// a6
	unsigned short trefi;	// s2
	unsigned short trfc;	// a5 / 8(sp)

	freq  = para->dram_clk;
	time  = para->dram_type;
	tpr13 = para->dram_tpr13;
	
	if (para->dram_tpr13 & 0x2)
	{
		//dram_tpr0
		tccd = ( (para->dram_tpr0 >> 21) & 0x7  ); // [23:21]
		tfaw = ( (para->dram_tpr0 >> 15) & 0x3f ); // [20:15]
		trrd = ( (para->dram_tpr0 >> 11) & 0xf  ); // [14:11]
		trcd = ( (para->dram_tpr0 >>  6) & 0x1f ); // [10:6 ]
		trc  = ( (para->dram_tpr0 >>  0) & 0x3f ); // [ 5:0 ]
		//dram_tpr1
		txp =  ( (para->dram_tpr1 >> 23) & 0x1f ); // [27:23]
		twtr = ( (para->dram_tpr1 >> 20) & 0x7  ); // [22:20]
		trtp = ( (para->dram_tpr1 >> 15) & 0x1f ); // [19:15]
		twr =  ( (para->dram_tpr1 >> 11) & 0xf  ); // [14:11]
		trp =  ( (para->dram_tpr1 >>  6) & 0x1f ); // [10:6 ]
		tras = ( (para->dram_tpr1 >>  0) & 0x3f ); // [ 5:0 ]
		//dram_tpr2
		trfc  = ( (para->dram_tpr2 >> 12)& 0x1ff); // [20:12]
		trefi = ( (para->dram_tpr2 >> 0) & 0xfff); // [11:0 ]
	}
	else {
		unsigned int frq2 = freq >> 2; // s0

		if (type == 3) {
			// DDR3
			trfc = auto_cal_timing( 350, frq2);
			tefi = auto_cal_timing(7800, frq2) / 32;
			twr  = auto_cal_timing(   8, frq2);
			trcd = auto_cal_timing(  15, frq2);
			twtr = twr;
			if (twtr < 2) twtr = 2;
			twr = trcd;
			if (twr < 2) twr = 2;
			if (dram_freq <= 800) {
				tfaw = auto_cal_timing(50, frq2);
				trrd = auto_cal_timing(10, frq2);
				if (trrd < 2) trrd = 2;
				trc  = auto_cal_timing(53, frq2);
				tras = auto_cal_timing(38, frq2);
				txp  = twtr; //  8
				trp  = trcd; // 15
			}
			else {
				tfaw = auto_cal_timing(35, frq2);
				trrd = auto_cal_timing(10, frq2);
				if (trrd < 2) trrd = 2;
				trcd = auto_cal_timing(14, frq2);
				trc  = auto_cal_timing(48, frq2);
				tras = auto_cal_timing(34, frq2);
				txp  = trrd; // 10
				trp  = trcd; // 14
			}
		}
		else if (type == 2) {
			// DDR2
			tfaw  = auto_cal_timing(  50, frq2);
			trrd  = auto_cal_timing(  10, frq2);
			trcd  = auto_cal_timing(  20, frq2);
			trc   = auto_cal_timing(  65, frq2);
			twtr  = auto_cal_timing(   8, frq2);
			trp   = auto_cal_timing(  15, frq2);
			tras  = auto_cal_timing(  45, frq2);
			trefi = auto_cal_timing(7800, frq2) / 32;
			trfc  = auto_cal_timing( 328, frq2);
			txp   = 2;
			twr   = trp; // 15
		}
		else if (type == 6) {
			// LPDDR2
			tfaw  = auto_cal_timing(  50, frq2);
			if (tfaw < 4) tfaw = 4;
			trrd  = auto_cal_timing(  10, frq2);
			if (trrd == 0) trrd = 1;
			trcd  = auto_cal_timing(  24, frq2);
			if (trcd < 2) trcd = 2;
			trc   = auto_cal_timing(  70, frq2);
			txp   = auto_cal_timing(   8, frq2);
			if (txp == 0) {
				txp  = 1;
				twtr = 2;
			}
			else {
				twtr = txp;
				if (txp < 2) {
					txp  = 2;
					twtr = 2;
				}
			}
			twr   = auto_cal_timing(  15, frq2);
			if (twr < 2) twr = 2;
			trp   = auto_cal_timing(  17, frq2);
			tras  = auto_cal_timing(  42, frq2);
			trefi = auto_cal_timing(3900, frq2) / 32;
			trfc  = auto_cal_timing( 210, frq2);
			txp = auto16; //@L73 - 2 lines
		}
		else if (type == 7) {
			// LPDDR3
			tfaw  = auto_cal_timing(  50, frq2);
			if (tfaw < 4) tfaw = 4;
			trrd  = auto_cal_timing(  10, frq2);
			if (trrd == 0) trrd = 1;
			trcd  = auto_cal_timing(  24, frq2);
			if (trcd < 2) trcd = 2;
			trc   = auto_cal_timing(  70, frq2);
			twtr   = auto_cal_timing(  8, frq2);
			if (twtr < 2) twtr = 2;
			twr   = auto_cal_timing(  15, frq2);
			if (twr < 2) twr = 2;
			trp   = auto_cal_timing(  17, frq2);
			tras  = auto_cal_timing(  42, frq2);
			trefi = auto_cal_timing(3900, frq2) / 32;
			trfc  = auto_cal_timing( 210, frq2);
			txp = twtr;
		}
		else {
			// default
			trfc  = 128;
			trp   =   6;
			trefi =  98;
			txp   =  10;
			twr   =   8;
			twtr  =   3;
			tras  =  14;
			tfaw  =  16;
			trc   =  20;
			trcd  =   6;
			trrd  =   3;
		}
		//assign the value back to the DRAM structure
		para->dram_tpr0 = (trc<<0) | (trcd<<6) | (trrd<<11) | (tfaw<<15) | 0x400000 ;
		para->dram_tpr1 = (tras<<0) | (trp<<6) | (twr<<11) | (trtp<<15) | (twtr<<20)|(txp<<23);
		para->dram_tpr2 = (trefi<<0) | (trfc<<12);
	}

	unsigned int tcksrx;	// t1
	unsigned int tckesr;	// t4;
	unsigned int trd2wr;	// t6
	unsigned int trasmax;	// t3;
	unsigned int twtp;	// s6 (was twr!)
	unsigned int tcke;	// s8
	unsigned int tmod;	// t0
	unsigned int tmrd;	// t5
	unsigned int tmrw;	// a1
	unsigned int t_rdata_en;// a4 (was tcwl!)
	unsigned int tcl;	// a0
	unsigned int wr_latency;// a7
	unsigned int tcwl;	// first a4, then a5
	unsigned int mr3:	// s0
	unsigned int mr2;	// t2
	unsigned int mr1;	// s1
	unsigned int mr0;	// a3
	unsigned int dmr3;	// 72(sp)
	unsigned int trtp;	// 64(sp)
	unsigned int dmr1;	// 56(sp)
	unsigned int twr2rd;	// 48(sp)
	unsigned int tdinit3;	// 40(sp)
	unsigned int tdinit2;	// 32(sp)
	unsigned int tdinit1;	// 24(sp)
	unsigned int tdinit0;	// 16(sp)

	dmr1 = para->dram_mr1;
	dmr3 = para->dram_mr3;

	switch (type) {

	case 2:	// DDR2
	L59:	{
		trasmax = freq / 30;
		if (freq < 409) {
			tcl        = 3;
			t_rdata_en = 1;
			mr0        = 0x06a3;
		}
		else {
			r_rdata_en = 2;
			tcl        = 4;
			mr0        = 0x0e73;
		}
		tmrd       =  2;
		twtp       =  twr + 5;
		tcksrx     =  5;
		tckesr     =  4;
		trd2wr     =  4;
		tcke       =  3;
		tmod       = 12;
		wr_latency =  1;
		mr3        =  0;
		mr2        =  0;
		tdinit0    = 200*freq + 1;
		tdinit1    = 100*freq / 1000 + 1;
		tdinit2    = 200*freq + 1;
		tdinit3    =   1*freq + 1;
		tmrw       =  0;
		twr2rd     = twtr + 5;
		mr1        = dmr1;
		break;
	}
		
	case 3:	// DDR3
	L57:	{
		trasmax = freq / 30;
		if (freq <= 800) {
			mr0        = 0x1c70;
			tcl        =  6;
			wr_latency =  2;
			tcwl       =  4;
			mr2        = 24;
		}
		else {
			mr0        = 0x1e14;
			tcl        =  7;
			wr_latency =  3;
			tcwl       =  5;
			mr2        = 32;
		}
		
		twtp    = tcwl + 2 + twtr;	// WL+BL/2+tWTR
		trd2wr  = tcwl + 2 + twr;	// WL+BL/2+tWR
		twr2rd  = tcwl + twtr;		// WL+tWTR
		
		tdinit0 = 500*freq + 1;		// 500 us
		tdinit1 = 360*freq / 1000 + 1;	// 360 ns
		tdinit2 = 200*freq + 1;		// 200 us
		tdinit3 =   1*freq + 1;		//   1 us
		
		if ((tpr13>>2) & 0x03 == 0x01 || freq < 912) {
			mr1    = dmr1;
			t_rdata_en = tcwl;	// a5 <- a4
			tcksrx = 5;
			tckesr = 4;
			trd2wr = 5;
		}
		else {
			mr1    = dmr1;
			t_rdata_en = tcwl;	// a5 <- a4
			tcksrx = 5;
			tckesr = 4;
			trd2wr = 6;
		}
		tmod = 12;
		tmrd =  4;
		tmrw =  0;
		mr3  =  0;
		break;
		}

	case 6:	// LPDDR2
	L61:	{
		trasmax	   = freq / 60;
		mr3	   = dmr3;
		twtp       =  twr + 5;
		mr2	   =  6;
		mr1	   =  5;
		tckesr	   =  5;
		trd2wr	   = 10;
		tcke	   =  2;
		tmod	   =  5;
		tmrd	   =  5;
		tmrw	   =  3;
		tcl        =  4;
		wr_latency =  1;
		tdinit0    = 200*freq + 1;
		tdinit1    = 100*freq / 1000 + 1;
		tdinit2    =  11*freq + 1;
		tdinit3    =   1*freq + 1;
		twr2rd	   = twtr + 5;
		tcwl       =   2;
		mr1	   = 195;
		mr0	   =   0;
		break;
	}

	case 7:	// LPDDR3
	L58+4:	{
		trasmax = freq / 60;
		if (freq < 800) {
			tcwl       =  4;
			wr_latency =  3;
			t_rdata_en =  6;
			mr2        = 12;
		}
		else {
			tcwl       =  3;
			tcke	   =  6;
			wr_latency =  2;
			t_rdata_en =  5;
			mr2        = 10;
		}
		twtp    = tcwl + 5;
		tcl     = 7;
		mr3     = dmr3;
		tcksrx  = 5;
		tckesr  = 5;
		trd2wr  = 13;
		tcke    = 3;
		tmod    = 12;
		tdinit0 = 400*freq + 1;
		tdinit1 = 500*freq / 1000 + 1;
		tdinit2 =  11*freq + 1;
		tdinit3 =   1*freq + 1;
		tmrw    = 5;
		twr2rd  = tcwl + twtr + 5;
		mr1	= 195;
		mr0	= 0;
		break;
	}

	default:
	L84:
		twr2rd  	= 8;	// 48(sp)
		tcksrx		= 4;	// t1
		tckesr		= 3;	// t4
		trd2wr		= 4;	// t6
		trasmax		= 27;	// t3
		twtp		= 12;	// s6
		tcke		= 2;	// s8
		tmod		= 6;	// t0
		tmrd		= 2;	// t5
		tmrw		= 0;	// a1
		tcwl		= 3;	// a5
		tcl		= 3;	// a0
		wr_latency	= 1;	// a7
		t_rdata_en	= 1;	// a4
		mr3		= 0;	// s0
		mr2		= 0;	// t2
		mr1		= 0;	// s1
		mr0		= 0;	// a3
		tdinit3 	= 0;	// 40(sp)
		tdinit2 	= 0;	// 32(sp)
		tdinit1 	= 0;	// 24(sp)
		tdinit0 	= 0;	// 16(sp)
		break;
	}
L60:
	if (mr2 < tcl - trp + 2) {
		mr2 = tcl - trp + 2;
	}
	
	// Update mode block when permitted
	if ((para->dram_mr0 & 0xffff0000) == 0) para->dram_mr0 = mr0;
	if ((para->dram_mr1 & 0xffff0000) == 0) para->dram_mr1 = mr1;
	if ((para->dram_mr2 & 0xffff0000) == 0) para->dram_mr2 = mr2;
	if ((para->dram_mr3 & 0xffff0000) == 0) para->dram_mr3 = mr3;
	
	// Set mode registers
	writel((void*)0x3103030, para->dram_mr0);
	writel((void*)0x3103034, para->dram_mr1);
	writel((void*)0x3103038, para->dram_mr2);
	writel((void*)0x310303c, para->dram_mr3);
	writel((void*)0x310302c, (para->dram_odt_en>>4) & 0x3); // ??
	
	// Set dram timing DRAMTMG0 - DRAMTMG5
	reg_val= (twtp<<24) | (tfaw<<16) | (trasmax<<8) | (tras<<0);
	writel((void*)0x3103058, reg_val);
	reg_val= (txp<<16) | (trtp<<8)|(trc<<0);
	writel((void*)0x310305c, reg_val);
	reg_val= (tcwl<<24) | (tcl<<16) | (trd2wr<<8) | (twr2rd<<0);
	writel((void*)0x3103060, reg_val);
	reg_val= (tmrw<<16) | (tmrd<<12)|(tmod<<0);
	writel((void*)0x3103064, reg_val);
	reg_val= (trcd<<24) | (tccd<<16) | (trrd<<8)|(trp<<0);
	writel((void*)0x3103068, reg_val);
	reg_val= (tcksrx<<24) | (tcksrx<<16) | (tckesr<<8)|(tcke<<0);
	writel((void*)0x310306c, reg_val);
	
	// Set two rank timing
	reg_val  = readl((void*)0x3103078);
	reg_val &= 0x0fff0000;
	reg_val |= (para->dram_clk <= 800) ? 0xf0007600 : 0xf0006600;
	reg_val |= 0x10;
	writel((void*)0x3103078, reg_val);
	
	// Set phy interface time PITMG0, PTR3, PTR4
	reg_val = (0x2<<24) | (t_rdata_en<<16) | (0x1<<8) | (wr_latency<<0);
	writel((void*)0x3103080, reg_val);
	writel((void*)0x3103050, ((tdinit0<<0)|(tdinit1<<20)));
	writel((void*)0x3103054, ((tdinit2<<0)|(tdinit3<<20)));
	
	// Set refresh timing and mode
	reg_val = (trefi<<16) | (trfc<<0);
	writel((void*)0x3103090, reg_val);
	reg_val = 0x0fff0000 & (trefi<<15)
	writel((void*)0x3103094, reg_val);
}


int ccm_get_sscg()
{
}


int ccm_set_pll_sscg()
{
}


int ccm_set_pll_ddr_clk()
{
}


int mctl_sys_init()
{
}


void mctl_com_init(__dram_para_t *para)
{
	
}


int mctl_phy_ac_remapping()
{
}


unsigned int mctl_channel_init(unsigned int ch_index, __dram_para_t *para)
{
}


int DRAMC_get_dram_size()
{
	unsigned int rval, temp, size0, size1;

	rval = readl(0x3102000);	// MC_WORK_MODE, low word
	
	temp  = (rval>>8) & 0xf;	// page size code
	temp += (rval>>4) & 0xf;	// row width code
	temp -= 14;
	temp += (rval>>2) & 0x3;	// bank number code
	size0 = 1 << temp;

	temp = rval & 0x3;		// rank number code
	if (temp == 0) {
		return size0;
	}
	
	rval = readl(0x3102004);	// MC_WORK_MODE, high word
	
	temp = rval & 0x3;
	if (temp == 0) {
		return 2 * size0;
	}

	temp  = (rval>>8) & 0xf;	// page size code
	temp += (rval>>4) & 0xf;	// row width code
	temp -= 14;
	temp += (rval>>2) & 0x3;	// bank number code
	size1 = 1 << temp;
	
	return size0 + size1;
}


int dqs_gate_detect(__dram_para_t *para)
{
	unsigned int rval, dx0, dx1;

	if (readl(0x3103010) & (1 << 22)) {

		dx0 = (readl(0x3103348) >> 24) & 0x3;
		dx1 = (readl(0x31033c8) >> 24) & 0x3;

		if (dx0 == 2) {
			rval  = para->dram_para2;
			rval &= 0xffff0ff0;
			if (dx0 != dx1) {
				rval |= 0x1;
				para->dram_para2 = rval;
				printf("[AUTO DEBUG] single rank and half DQ!");
				return 1;
			}
			para->dram_para2 = rval;
			printf("[AUTO DEBUG] single rank and full DQ!");
			return 1;
		}
		else if (dx0 == 0) {
			rval  = para->dram_para2;
			rval &= 0xfffffff0;
			rval |= 0x00001001;
			para->dram_para2 = rval;
			printf("[AUTO DEBUG] dual rank and half DQ!");
			return 1;
		}
		else {
			if (para->dram_tpr13 & (1 << 29)) {
				printf("DX0 state:%d", dx0);
				printf("DX1 state:%d", dx1);
			}
			return 0;
		}
	}
	else {
		rval  = para->dram_para2;
		rval &= 0xfffffff0;
		rval |= 0x00001000;
		para->dram_para2 = rval;
		printf("[AUTO DEBUG] two rank and full DQ!");
		return 1;
	}
}


#define SDRAM_BASE	((unsigned int *)0x40000000)

int dramc_simple_wr_test(int mem_mb, int len)
{
	unsigned int  offs = (mem_mb >> 1) << 18; // half of memory size
	unsigned int  patt1 = 0x01234567;
	unsigned int  patt2 = 0xfedcba98;
	unsigned int *addr, v1, v2;
	
	addr = SDRAM_BASE;
	for (i = 0; i != len; i++, addr++) {
		writel(addr,        patt1 + i);
		writel(addr + offs, patt2 + i);
	}
	
	addr = SDRAM_BASE;	
	for (i = 0; i != len; i++) {
		v1 = readl(addr+i);
		v2 = patt1 + i;
		if (v1 != v2) {
			printf("DRAM simple test FAIL.\n");
			printf("%x != %x at address %x\n", v1, v2, addr+i);
			return 1;
		}
		v1 = readl(addr+offs+i);
		v2 = patt2 + i;
		if (v1 != v2) {
			printf("DRAM simple test FAIL.\n");
			printf("%x != %x at address %x\n", v1, v2, addr+offs+i);
			return 1;
		}
	}
	printf("DRAM simple test OK.\n");
	return 0;
}


void mctl_vrefzq_init(__dram_para_t *para)
{
	if (para->dram_tpr13 & (1 << 17) == 0) {
		rval  = readl(0x3103110) & 0x80808080;	// IOCVR0
		rval |= para->dram_tpr5;
		writel(0x3103110, rval);
		
		if ((para->dram_tpr13 & (1 << 16) == 0) 
			rval  = readl(0x3103114) & 0xffffff80; // IOCVR1
			rval |= para->dram_tpr6 & 0x7f;
			writel(0x3103114, rval);
		}
	}
}


int mctl_core_init(__dram_para_t *para)
{
	mctl_sys_init(para);
	mctl_vrefzq_init(para);
	mctl_com_init(para);
	mctl_phy_ac_remapping(para);
	auto_set_timing_para(para);
	return mctl_channel_init(0, para);
}

#define RAM_BASE = ((char*)0x40000000)

int auto_scan_dram_size(__dram_para_t *para) // s7
{
	unsigned int	rval, i, j, rank, maxrank, pgsize, offs;
	char		*chk, *ptr, *mc_work_mode;

	if (mctl_core_init(para) == 0) {
		printf("[ERROR DEBUG] DRAM initialisation error : 0!");
		return 0;
	}

	maxrank = (para->dram_para2 & 0xf000) ? 1 : 2;
	mc_work_mode = (char*)0x3102000;
	offs = 0;
	
	// write test pattern
	for (i = 0, ptr = RAM_BASE; i < 64; i++, ptr += 4) {
		writel((void*)ptr, (i & 1) ? ptr : ~ptr);
	}

	for (rank = 0; rank < maxrank; ) {

		// Set row mode
		rval  = readl(mc_work_mode);
		rval &= 0xfffff0f3;
		rval |= 0x000006f0;
		writel(0x3102000, rval);
		while (readl(mc_work_mode) != rval);
		
		// Scan per address line, until address wraps (i.e. see shadow)
		for(i = 11; i < 17; i++) {
			chk = RAM_BASE + (1 << (i + 11));
			ptr = RAM_BASE;
			for (j = 0; j < 64; j++) {
				if (readl((void*)chk) != ((j & 1) ? ptr : ~ptr))
					goto out1;
				ptr += 4;
				chk += 4;
			}
			break;
		out1:
		}
		if (i > 16) i = 16;	
		printf("[AUTO DEBUG] rank %d row = %d", rank, i);
		
		// Store rows in para 1
		shft = 4 + offs;
		rval = para->dram_para1;
		rval &= ~(0xff << shft);
		rval |= i << shft;
		para->dram_para1 = rval
		
		if (rank == 1) {
			// Set bank mode for rank0
			rval  = readl(0x3102000);
			rval &= 0xfffff003;
			rval |= 0x000006a4;
			writel(0x3102000, rval);
		}
	
		// Set bank mode for current rank
		rval  = readl(mc_work_mode);
		rval &= 0xfffff003;
		rval |= 0x000006a4;
		writel(mc_work_mode, rval);
		while (readl(mc_work_mode) != rval);
		
		chk = RAM_BASE;
		ptr = RAM_BASE;
		for (i = 0, j = 0; i < 64; i++) {
			if (readl((void*)chk) != ((j & 1) ? ptr : ~ptr)) {
				j = 1;
				break;
			}
		}
		banks = (j + 1) << 2; // 4 or 8
		printf("[AUTO DEBUG] rank %d bank = %d", rank, banks);
		
		// Store banks in para 1
		shft = 12 + offs;
		rval  = para->dram_para1;
		rval &= ~(0xf << shft);
		rval |= j << shft;
		para->dram_para1 = rval;
		
		if (rank == 1) {
			// Set page mode for rank0
			rval  = readl(0x3102000);
			rval &= 0xfffff003;
			rval |= 0x00000aa0;
			writel(0x3102000, rval);
		}

		// Set page mode for current rank
		rval  = readl(mc_work_mode);
		rval &= 0xfffff003;
		rval |= 0x00000aa0;
		writel(mc_work_mode, rval);
		while (readl(mc_work_mode) != rval);
		
		// Scan per address line, until address wraps (i.e. see shadow)
		for(i = 9; i < 14; i++) {
			chk = RAM_BASE + (1 << (i + 11));
			ptr = RAM_BASE;
			for (j = 0; j < 64; j++) {
				if (readl((void*)chk) != ((j & 1) ? ptr : ~ptr))
					goto out2;
				ptr += 4;
				chk += 4;
			}
			break;
		out2:
		}
		if (i > 13) i = 13;
		int pgsize = (i==9) ? 0 : (1 << (i-10));
		printf("[AUTO DEBUG] rank %d page size = %d KB", rank, pgsize);
		
		// Store page size
		shft = offs;
		rval  = para->dram_para1;
		rval &= ~(0xf << shft);
		rval |= i << shft;
		para->dram_para1 = rval;
		
		// Move to next rank
		rank++;
		if (rank != maxrank) {
			if (rank == 1) {
				rval  = readl(0x3202000); // MC_WORK_MODE
				rval &= 0xfffff003;
				rval |= 0x000006f0;
				writel(0x3202000);
				
				rval  = readl(0x3202004); // MC_WORK_MODE2
				rval &= 0xfffff003;
				rval |= 0x000006f0;
				writel(0x3202004);
			}
			offs += 16; // store rank1 config in upper half of para1
			mc_work_mode +=  4; // move to MC_WORK_MODE2
		}
	}
	if (maxrank == 2) {
		para_>dram_para2 &= 0xfffff0ff;
		// note: rval is equal to para->dram_para1 here
		if ((rval & 0xffff) == ((rval >> 16) & 0xffff)) {
			printf("rank1 config same as rank0");
		}
		else {
			para_>dram_para2 |= 0x00000100;
			printf("rank1 config different from rank0");
		}
	}
	return 1;	
}


int auto_scan_dram_rank_width(__dram_para_t *para)
{
	unsigned int s2 = para->dram_para1;
	unsigned int s1 = para->dram_tpr13;
	unsigned int v;
	
	para->dram_para1 = 0xb000b0;
	v = (para->dram_para2 & 0xfffffff0) | 0x1000;
	para->dram_para2 = v;

	v = (s1 & 0xfffffff7) | 0x5;
	para->dram_tpr13 = v;
	
	mctl_core_init(para);
	if (readl(0x3103010) & (1 << 20)) {
		return 0;
	}
	if (dqs_gate_detect(para) == 0) {
		return 0;
	}

	para->dram_tpr13 = s1;
	para->dram_para1 = s2;
	return 1;
}


int auto_scan_dram_config(__dram_para_t *para)
{
	if ((para->dram_tpr13 & (1 << 14) == 0) &&
	    (auto_scan_dram_rank_width(para) == 0))
	{
		printf("[ERROR DEBUG] auto scan dram rank & width failed !");
		return 0;
	}
	if (para->dram_tpr13 & (1 << 0) == 0)  &&
	    (auto_scan_dram_size(para) == 0 ))
	{
		printf("[ERROR DEBUG] auto scan dram size failed !");
		return 0;
	}
	if (para->dram_tpr13 & (1 << 15) == 0) {
		para->dram_tpr13 |= 0x6003;
		return 1;
	}
}


signed int init_DRAM(int type, __dram_para_t *para)
{
	// s0 = para;

	if ( (para->tpr13 << 47) >= 0 ) {
		writel(ANALOG_SYS_PWROFF_GATING_REG, 0);
		writel(RES_CAL_CTRL_REG, readl(RES_CAL_CTRL_REG) & -4);
		__usdelay(10);
		writel(RES_CAL_CTRL_REG, readl(RES_CAL_CTRL_REG) & -265);
		__usdelay(10);
		writel(RES_CAL_CTRL_REG, readl(RES_CAL_CTRL_REG) | 1);
		__usdelay(20);

		printf("ZQ value = 0x%x***********", readl(0x3000172));
	}
	else {
		printf("DRAM only have internal ZQ!!");
		writel(RES_CAL_CTRL_REG, readl(RES_CAL_CTRL_REG) | 256 );
		writel(0x300016e, 0);
		__usdelay(10);
	}

	s1 = get_pmu_exists();
	printf("get_pmu_exist() = %d", s1);
	if ( s1<0 ) {
		dram_vol_set(para);
	}
	else {
		if (para->dram_type==2) set_ddr_voltage(1800); else
		if (para->dram_type==3) set_ddr_voltage(1500);
	}

	if ( (para->dram_tpr13 & 1)==0 ) {
		if ( auto_scan_dram_config(para)==0 ) {
			return 0;
		}
	}
	printf("DRAM BOOT DRIVE INFO: %s", "V0.24");
	printf("DRAM CLK = %d MHz", para->dram_clk);
	printf("DRAM Type = %d (2:DDR2,3:DDR3)", para->dram_type);
	if ( para->dram_odt_en == 0 ) {
		printf("DRAMC read ODT  off.");
	}
	else {
		printf("DRAMC ZQ value: 0x%x", para->dram_zq);
	}
	
	a5 = para->dram_mr1 & 0x44;
	if ( a5==0 ) {
		printf("DRAM ODT off.");
	}
	else {
		printf("DRAM ODT value: 0x%x.", a5);
	}	

	if ( mtcl_core_init(para)==0 ) {
		printf("DRAM initial error : 1 !");
		return 0;
	}

	s1 = para->dram_para2;
	if ( s1<0 ) {
		s1 = (0x7fff0000U & s1) >> 16
	}
	else {
		s1 = DRAMC_get_ram_size();
		printf("DRAM SIZE =%d M", s1);
		para->dram_para2 = (para->dram_para2 & 0xffffu) | s1 << 16;
	}
@12db4	
	if ( (para->dram_tp14 << 33)<0 ) {
		fence_ir();
		a5 = para->dram_tpr8;
		if ( a5==0 ) {
			a5 = 0x10000200;
		}
@12ddc		writel(0x31030a0, a5);
		writel(0x310309c, 0x422);
		writel(0x3103004, readl(0x3103004) | 1 );
		printf("Enable Auto SR");
	}
@12f34  else {
		writel(0x31030a0, readl(0x31030a0) & 0xffff0000);
		writel(0x3103004, readl(0x3103004) & (~1) );
	}

@12e0c	a5 = readl(0x3103100) & ~(0xf000);
	if ( (a4 = para->tpr13 & 0x200)==0 ) {
		if ( para->dram_type != 6 ) { ->12f68
			writel(0x3103100, a5);
		}
	}
	writel(0x3103100, a5 | 0x5);
@12e50

			
		}

@12d38
	return 0;
}

// Apparently only used for FPGA testing
//
unsigned int mctl_init(void)
{
	signed int ret_val=0;
	
	__dram_para_t dram_para;

	// Build parameter block
	dram_para.dram_clk	= 528;
	dram_para.dram_type	= 2;
	dram_para.dram_zq	= 0x7b7bf9;
	dram_para.dram_odt_en	= 0;
	dram_para.dram_para1	= 210;
	dram_para.dram_para2	= 0;
	dram_para.dram_mr0	= 0x0e73;
	dram_para.dram_mr1	= 0x2;
	dram_para.dram_mr2	= 0;
	dram_para.dram_mr3	= 0;
	dram_para.dram_tpr0 	= 0x00471992;
	dram_para.dram_tpr1 	= 0x0131a10c;
	dram_para.dram_tpr2 	= 0x80000000;
	dram_para.dram_tpr3 	= 0xffffffff;
	dram_para.dram_tpr4	= 0;
	dram_para.dram_tpr5	= 0x48484848;
	dram_para.dram_tpr6	= 0x48;
	dram_para.dram_tpr7	= 0x1621121e;
	dram_para.dram_tpr8	= 0;
	dram_para.dram_tpr9	= 0;
	dram_para.dram_tpr10	= 0;
	dram_para.dram_tpr11	= 0x40000000;
	dram_para.dram_tpr12	= 0x45;
	dram_para.dram_tpr13	= 0x34000000;

	ret_val = init_DRAM(0, &dram_para);
	return ret_val;
}

