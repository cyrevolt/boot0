#!/bin/sh

echo "Building a condensed listing file"

cpp mctl_hal.S >mctl_hal.i
riscv64-unknown-elf-as -march=rv64g -g mctl_hal.i -o ctl.o
riscv64-unknown-elf-gcc -g harness.c ctl.o -o ctl.out
./objdumpmap.py ctl.out >lst
rm mctl_hal.i ctl.o ctl.out

